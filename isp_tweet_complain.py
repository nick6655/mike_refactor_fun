#!bin/python2

import json
import speedtest

class ISPSpeeds(object):

    def __init__(self):

        with open('isp_profile.jsn') as f:
            data = json.load(f)

        self.name = data.get('name')
        self.upload = data.get('upload_speed')
        self.download = data.get('download_speed')

class CurrentSpeeds(object):

    def __init__(self):

        speed_test = None
        results_dict = None
    
        try:
            speed_test = speedtest.Speedtest()
            speed_test.get_best_server()
            speed_test.download()
            speed_test.upload()
            speed_test.results.share()
            results_dict = speed_test.results.dict()
        except speedtest.ConfigRetrievalError:
            raise RuntimeError('Internet likely down.')
        except Exception as e:
            raise(e)

        if 'client' in results_dict:
            self.isp = results_dict['client'].get('isp')
    
        self.ping = results_dict.get('ping')
        self.upload = results_dict.get('upload')
        self.download = results_dict.get('download')
        self.timestamp = results_dict.get('timestamp')

    @property
    def upload(self):
        return self.__upload

    @upload.setter
    def upload(self, value):
        if type(value) == type(float()):
            self.__upload = round(value / 1000000)
        else:
            self.__upload = None 

    @property
    def download(self):
        return self.__download

    @download.setter
    def download(self, value):
        if type(value) == type(float()):
            self.__download = round(value / 1000000)
        else:
            self.__download = None


class CompSpeeds(object):

    @staticmethod
    def check_speeds(isp_speeds, cur_speeds, threshold=0.90):
        
        download = True
        upload = True

        if isp_speeds.download * threshold > cur_speeds.download:
            download = False

        if isp_speeds.upload * threshold > cur_speeds.upload:
            upload = False

        return dict(upload=upload, download=download)

isp_speeds = ISPSpeeds()
cur_speeds = CurrentSpeeds()

print cur_speeds.upload
print cur_speeds.download

print CompSpeeds.check_speeds(isp_speeds, cur_speeds, threshold=0.50)
